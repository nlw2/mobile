import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { View, Image, Text } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';

import heartIcon from '../../assets/images/icons/heart-outline.png';
import unfavoriteIcon from '../../assets/images/icons/unfavorite.png';
import whatsAppIcon from '../../assets/images/icons/whatsapp.png';

import styles from './styles';

function TeacherItem (){

    return (
        <View style={styles.container}>
            <View style={styles.profile}>
                <Image 
                    source={{uri: 'https://scontent.fbsb3-1.fna.fbcdn.net/v/t31.0-8/21414970_1511852282226282_6953448256275863185_o.jpg?_nc_cat=100&_nc_sid=09cbfe&_nc_eui2=AeFZlbojxLDqjmLgkyG4ifbt2dW-8esCJLbZ1b7x6wIktoKzv46f1PWYOA5a2buoiV6vnEHk6tkepuTSUMiZD4QN&_nc_ohc=tnt_l0zLXMAAX_qOzqr&_nc_ht=scontent.fbsb3-1.fna&oh=d886888b247d6f63db3139c936e85176&oe=5F548FE6'}}
                    style={styles.avatar}
                />

                <View style={styles.profileInfo}>
                    <Text style={styles.name}>Larissa Loiola</Text>
                    <Text style={styles.subject}>Legal Tech</Text>
                </View>
            </View>

            <Text style={styles.bio}>Especialista em Legal Tech</Text>

            <View style={styles.footer}>
                <Text style={styles.price}>
                    Preço/hora {'   '}
                    <Text style={styles.priceValue}>R$120,00</Text>
                </Text>
            

                <View style={styles.buttonsContainer}>
                    <RectButton style={[styles.favoriteButton, styles.favorited]}>
                        {/* <Image source={heartIcon} /> */}
                        <Image source={unfavoriteIcon} />
                    </RectButton>

                    <RectButton style={styles.contactButton}>
                        <Image source={whatsAppIcon} />
                        <Text style={styles.contactButtonText}>Entrar em contato</Text>
                    </RectButton>
                </View>
            </View>
        </View>
    );
}

export default TeacherItem;